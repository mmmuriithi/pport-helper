<?php namespace pPort\Services\Helper;
use pPort;
class Provider extends pPort\Provider {

    public $alias='helper';

    public function register()
    {
    	 return "\\pPort\\Services\\Helper\\HelperService";
    }
}