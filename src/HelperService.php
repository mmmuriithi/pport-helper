<?php namespace pPort\Services\Helper;
use pPort;
class HelperService extends pPort\Service {

   public function load_all()
   {
        foreach (glob('Helpers/*.php') as $helper){
            require_once($helper);
        }
   }

   public function load($helper)
   {
      require_once('helpers'.DS.$helper.'.php'); 
   }
}
